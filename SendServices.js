const amqp = require("amqplib");

console.log("Masukan pesan:");
process.stdin.once("data", (crunk) => {
  let pesanan = crunk.toString().trim();
  console.log("Pesan terkirim " + pesanan + "!");

  amqp
    .connect("amqp://localhost")
    .then((conn) => {
      return conn
        .createChannel()
        .then((channel) => {
          const queue = "Pesan";
          const message = pesanan;
          const ok = channel.assertQueue(queue, { durable: false });
          return ok.then(() => {
            channel.sendToQueue(queue, Buffer.from(message));
            return channel.close();
          });
        })
        .finally(() => conn.close());
    })
    .catch(console.warn);
});
